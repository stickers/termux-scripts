#!/bin/bash

rm test.zip
zip -j test.zip  test/*

strings -t d test.zip | \
grep '^\s\+[[:digit:]]\+\sOstic\w\+\.webp' | \
sed 's/^\s\+\([[:digit:]]\+\)\s\(Ostic\)\(\w\+\.webp\).*$/\1 \2\3 \/stic\3/g' | \
while read -a line; do
  line_nbr=${line[0]};
  fname=${line[1]};
  new_name=${line[2]};
  len=${#fname};
  printf "line: "$line_nbr"\nfile: "$fname"\nnew_name: "$new_name"\nlen: "$len"\n";
  dd if=<(printf $new_name"\n") of=test.zip bs=1 seek=$line_nbr count=$len conv=notrunc  
done;

strings -t d test.zip | \
grep '^\s\+[[:digit:]]\+\sOauth\w\+\.txt' | \
sed 's/^\s\+\([[:digit:]]\+\)\s\(Oauth\)\(\w\+\.txt\).*$/\1 \2\3 \/auth\3/g' | \
while read -a line; do
  line_nbr=${line[0]};
  fname=${line[1]};
  new_name=${line[2]};
  len=${#fname};
  printf "line: "$line_nbr"\nfile: "$fname"\nnew_name: "$new_name"\nlen: "$len"\n";
  dd if=<(printf $new_name"\n") of=test.zip bs=1 seek=$line_nbr count=$len conv=notrunc  
done;

strings -t d test.zip | \
grep '^\s\+[[:digit:]]\+\sOtit\w\+\.txt' | \
sed 's/^\s\+\([[:digit:]]\+\)\s\(Otit\)\(\w\+\.txt\).*$/\1 \2\3 \/tit\3/g' | \
while read -a line; do
  line_nbr=${line[0]};
  fname=${line[1]};
  new_name=${line[2]};
  len=${#fname};
  printf "line: "$line_nbr"\nfile: "$fname"\nnew_name: "$new_name"\nlen: "$len"\n";
  dd if=<(printf $new_name"\n") of=test.zip bs=1 seek=$line_nbr count=$len conv=notrunc  
done;

strings -t d test.zip | \
grep '^\s\+[[:digit:]]\+\sOtra\w\+\.png' | \
sed 's/^\s\+\([[:digit:]]\+\)\s\(Otra\)\(\w\+\.png\).*$/\1 \2\3 \/tra\3/g' | \
while read -a line; do
  line_nbr=${line[0]};
  fname=${line[1]};
  new_name=${line[2]};
  len=${#fname};
  printf "line: "$line_nbr"\nfile: "$fname"\nnew_name: "$new_name"\nlen: "$len"\n";
  dd if=<(printf $new_name"\n") of=test.zip bs=1 seek=$line_nbr count=$len conv=notrunc  
done;

cp test.zip file/test.wastickers
