#!/bin/bash
echo STICKER ENCODER!
T=$(termux-dialog text -t "Enter message:" -p | jq '.text' | sed 's/\"//g')
C=$(termux-dialog sheet -v "Big Dog,Smal Dog,Black Cat,White Cat,Angry Cat, Fish In Bowl" -t "Chose sticker to encode")
if [[ "$C" =~ .*"x\": 0".* ]]; then
	bash code.sh Osticker_1.webp $T
fi
if [[ "$C" =~ .*"x\": 1".* ]]; then
        bash code.sh Osticker_2.webp $T
fi
if [[ "$C" =~ .*"x\": 2".* ]]; then
        bash code.sh Osticker_3.webp $T
fi
if [[ "$C" =~ .*"x\": 3".* ]]; then
        bash code.sh Osticker_4.webp $T
fi
if [[ "$C" =~ .*"x\": 4".* ]]; then
        bash code.sh Osticker_5.webp $T
fi
if [[ "$C" =~ .*"x\": 5".* ]]; then
        bash code.sh Osticker_6.webp $T
fi

bash batch.sh
termux-share -a send file/test.wastickers
