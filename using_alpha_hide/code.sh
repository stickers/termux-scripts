#!/bin/bash

file=$1
message=$2

rm test/$file
cp  sticker_backup/$file test/$file
dwebp test/$file -o test/tmp.png

python3 a_hide.py -f test/tmp.png -d $message -o test/tmp2.png

#convert  _test/tmp.png -fuzz 2% -transparent black _test/tmp2.png
rm test/$file
cwebp -lossless test/tmp2.png -o test/$file
python3 a_unhide.py -f test/tmp2.png
dwebp test/$file -o test/tmp.png
python3 a_unhide.py -f test/tmp.png
rm test/tmp*
