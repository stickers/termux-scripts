from PIL import Image
from random import choice
from optparse import OptionParser

def isTransparentPixel(img, x: int, y: int) -> bool:
    (r, g, b, a) = img.getpixel((x,y))
    if(a == 0):
        return True
    else:
        return False

def setLSB(v, state):
    if state == "0":
        return v & 0b11111110
    elif state == "1":
        return v | 0b00000001
    else:
        print(f"invalide state: {state}")
        return v

def toBin(string):
    return ''.join(format(x, 'b').zfill(8) for x in string)

def hide(data, imgName, outName, startingPixel=(0,0)):
    """
    Hides the string data in the image imgName and creates a new image containing the data outName.
    startingPixel is optional and will be choosed randomly if not specified.

    @param data: Data to hide.
    @type data: String

    @param imgName: Name of the original image.
    @type imgName: String

    @param outName: Name of the resulting image.
    @type outName: String

    @param startingPixel: Optional starting pixel coordinates.
    @type startingPixel: Tuple

    @returns: The starting pixel used.
    @rtype: Tuple
    """
    img = Image.open(imgName)
    # The number of pixels in the image
    total = img.size[0] * img.size[1]
    AVAILABLE = []
    #Check for coordinates which are transparent, coordinates are stored in AVAILABLE
    for i in range(img.size[0]):
        for j in range(img.size[1]):
            if(isTransparentPixel(img, i, j)):
                AVAILABLE.append((i, j))


    img_capacity = len(AVAILABLE)*3
    #tmp_bits temporarily stores bits
    tmp_bits = [bin(t)[2:] for t in data]
    #bits_to_hide contains all bits of the data
    bits_to_hide = ["0" * (8 - len(t)) + t if len(t)
                        < 8 else t for t in tmp_bits]
    if(len(bits_to_hide) > img_capacity):
        print("bits_to_hide is greater than capacity")
        raise SystemExit
    
    for i in range(0, len(bits_to_hide), 3):
        #Choose coordinate to hide data, we can hide 3 bytes
        if(i+2 > len(bits_to_hide)-1):
            break
        coord = AVAILABLE[0]
        x = coord[0]
        y = coord[1]
        AVAILABLE.remove(coord)
        r = int(bits_to_hide[i], 2)
        g = int(bits_to_hide[i+1], 2)
        b = int(bits_to_hide[i+2], 2)
        img.putpixel((x,y), (r, g, b, 0))

    #Signify end of data by putting 0, 0, 0, 0
    coord = AVAILABLE[0]
    x = coord[0]
    y = coord[1]
    img.putpixel((x,y), (0,0,0,0))
    img.save(outName)
    img.close()
    return True

def get_options():
    parser = OptionParser()
    # required
    parser.add_option("-f", "--inputfile", type="string", help="Input file in witch data should be hidden.")
    parser.add_option("-d", "--data", type="string", help="Data (represented as string) to hide.")
    parser.add_option("-s", "--secretfile", type="string", help="Secret file to hide.")
    # Optionals
    parser.add_option("-o", "--outputfile", type="string", default="out.png", help="Name of the output file containing the hidden data.")
    parser.add_option("-x", type=float, help="Starting pixel's x coordinate.")
    parser.add_option("-y", type=float, help="Starting pixel's y coordinate.")
    (options, args) = parser.parse_args()

    if len(args) != 0 or not options.inputfile or (not options.data and not options.secretfile):
        parser.print_help()
        raise SystemExit

    if options.secretfile and options.data:
        print("Only one of --secretfile (-s) or --data (-d) should be provided.")
        parser.print_help()
        raise SystemExit

    if options.secretfile:
        with open(options.secretfile, "rb") as f:
            options.data = f.read()

    # force bytes
    if type(options.data) == str:
        options.data = options.data.encode()

    return options

if __name__ == '__main__':
    options = get_options()

    if options.x and options.y:
        print(hide(options.data, options.inputfile, options.outputfile, (options.x, options.y)))
    else:
        print(hide(options.data, options.inputfile, options.outputfile))
